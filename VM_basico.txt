############ - Receita
#
# Montada por Juliano - USP - STI - CeTI-SP - DVTIN - SCTIN - SCTS
#
### Objetivo: Check-list para instalação de servidores.
#
# Data Criacao:      		Marco/2023
# Data Ultima alteracao:	10/Mar/2023	- jguidini
#
# Versao: 1 - Baseada nas receitas internas (10/03/2023) - jguidini
#
# Criada em: Universidade de Sao Paulo - USP
# Superintendencia de Tecnologia da Informacao - STI
# Centro de Tecnologia de Informacao da Sao Paulo - CeTI-SP
# Divisao Tecnica de Infraestrutura - DVTIN
# Secao Tecnica de Internuvem - SCTIN
# Setor Tecnico de Software - SCTS
#
# Contato: internuvem@usp.br
#
### Breve descricao
#
# Servir como um guia para configuracao basica de uma maquina linux.
#
### Essa receita
#
# Foi escrita sem sinais graficos da lingua portuguesa, mas certamente nao
# imune a erros ortografico e de digitacao. O motivo e que ao ler em shell
# evita-se qualquer problema, afinal nunca se sabe o que poderemos
# ter que resolver e em que situacao estaremos. Dessa forma erros de
# visualizacao sao reduzidos. Ao mesmo tempo garante a nao existencia de
# caracteres especiais, entao pode copiar da receita e colar no shell sem
# preocupacao.
#
# O texto explicativo foi contido dentro das primeiras 80 colunas para que
# pudesse ser lido em meia tela (supondo um terminal na outra meia tela).
# Os comandos nao foram quebrados em 80 colunas, para nao dar problema
# na execucao em caso de copia daqui para o terminal.
#
############# O que falta
#
# Firewall
# ajuste de hora
# Criacao de usuarios
#
#############

Imagino que a maquina sera instanciada em nuvem, mas isso nao importa.
O procedimento e o mesmo, via de regra.

############ SOs

Ubuntu 18.04 LTS
Ubuntu 20.04 LTS
Debian 10
Debian 11

Vale para outros? Sim, claro, o principio e o mesmo, mas vou me ater a linha
Debian.

Configure algum acesso via ssh para facilitar a vida.

############ Atualiza tudo

Configurou o sources.list? Vamos la:

# Entendo a sintaxe:
# Na pratica, ou seja, nao usei as definicoes do manual.

deb link do mirror/debian <distro> <repos>

A entrada deb-src e para baixar os fontes dos pacotes, nao precisa, mas se tiver
tudo bem.

link do mirror -> O site que tem o mirror, por exemplo: http://linorg.usp.br
Distro -> Qual o codnome do SO, por exemplo: Debian 11 -> bullseye
repos -> Qual parte do repositorio vamos usar, por exemplo: main contrib non-free

# Entao montando as linhas temos:
deb http://linorg.usp.br/debian bullseye main contrib non-free
deb-src http://linorg.usp.br/debian bullseye main contrib non-free
	
# Existem outros repositorios, como atualizacoes:
deb http://linorg.usp.br/debian bullseye-updates main contrib non-free
deb-src http://linorg.usp.br/debian bullseye-updates main contrib non-free
    
# E atualizacoes de seguranca:
deb http://linorg.usp.br/debian-security bullseye-security main contrib non-free
deb-src http://linorg.usp.br/debian-security bullseye-security main contrib non-free

# Configure o /etc/sources.list
vi /etc/sources.list

# Atualizar e manter atualizado o SO e vital.

apt-get update
apt-get upgrade -y
apt-get full-upgrade -y
apt autoremove -y
reboot

############ Configure o /etc/hosts

Ajuste o arquivo para refletir o nome da maquina, assim:

vi /etc/hosts

127.0.0.1       localhost
143.107.0.1     servidor.uspnet.usp.br       servidor

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

############ Configure o /etc/hostname

O /etc/hostname deve conter o nome da maquina, assim

vi /etc/hostname
servidor

############ Desligando o systemd-resolved

As vezes o systemd-resolved mais atrapalha que ajuda, cabe
a voce julgar se deve desligar ou nao. Existem casos em que 
nao e razoavel manter ele ligado, por exemplo, em um servidor
DNS, pois provavelmente estara enfileirando mais caches do que 
seja necessario.

# Debian 10 e 11 nao vem habilitado o systemd-resolved
# Isso e coisa de Ubuntu.. srsr

# Pare o systemd-resolved
systemctl disable systemd-resolved
systemctl stop systemd-resolved

# Remove o link
unlink /etc/resolv.conf 

# Cria novo resolv.conf, por exemplo assim:
vi /etc/resolv.conf

search xyz.usp.br
nameserver 100.100.100.100
nameserver 100.100.100.101

# Se precisar, ajusta a permissao e propriedade
chmod 644 /etc/resolv.conf
chown root:root /etc/resolv.conf

############ Configure o /etc/resolv.conf

Alem dele configurar os DNSs e daqui que vem o sufixo
do nome da maquina.

# Exemplo - Inicio
vi /etc/resolv.conf

search uspnet.usp.br usp.br linorg.usp.br
nameserver 100.100.100.100
nameserver 100.100.100.101
# Exemplo - Fim

## Entendendo o que configurou

A maquina tera o FQDN (Full Qualified Domain Name): servidor.uspnet.usp.br
O nome servidor veio do /etc/hostname e o sufixo do search.

E sempre que uma busca ao DNS for feita e nao for informado o FQDN
o sistema tentara:

nomedohost.uspnet.usp.br
nomedohost.usp.br
nomedohost.linorg.usp.br

E ela encaminhara para algum dos DNSs recursivos listados por nameserver.

############ Se tem IPv6, configure, senao desligue - Inicio

# Edite o conf e adicione a configuracao na ultima linha.
vi /etc/sysctl.conf 

net.ipv6.conf.all.disable_ipv6 = 1

# Aplique a configuracao
sysctl -p

# Teste 
ip a

############ Se tem IPv6, configure, senao desligue - Fim

############ Da um reforço no SSH - Inicio

Vamos tornar o ssh mais seguro, impondo algoritmos melhores para a conexao.

vi /etc/ssh/sshd_config

###### Custom USP - Inicio

Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
KexAlgorithms curve25519-sha256@libssh.org,curve25519-sha256,diffie-hellman-group-exchange-sha256,diffie-hellman-group18-sha512,diffie-hellman-group16-sha512
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com
HostKeyAlgorithms ssh-ed25519-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ssh-ed25519,rsa-sha2-512,rsa-sha2-256

X11Forwarding no
PermitRootLogin no
AllowTcpForwarding no
AllowStreamLocalForwarding no
GatewayPorts no
PermitTunnel no
PasswordAuthentication no

###### Custom USP - Fim

# Testa
sshd -t

# Reinicia o sshd se passou no teste
systemctl restart sshd

##### Do lado cliente sugiro:
ssh-keygen -b 2048 -t ed25519 -a 100

# Poe senha na chave!! Vai que.. 
# Login somente por chave (com senha) e depois sudo.
Para remover o login por senha, edite novamente o /etc/ssh/sshd_config
e desligue a opcao PasswordAuthentication no

# Vez por outra teste a qualidade da criptografia
Utilize o software https://github.com/jtesta/ssh-audit para testar o ssh.

############ Da um reforço no SSH - Fim

############ Fail2ban - Inicio

# https://github.com/fail2ban/fail2ban

apt-get install fail2ban -y

cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
vi /etc/fail2ban/jail.local

# Ajuste assim, como sugestao
findtime  = 60m
maxretry = 3
usedns = no

[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.
#mode   = normal
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
enabled = true

# Reinicie
fail2ban-client reload --restart

# Verifique
fail2ban-client status sshd

############ Fail2ban - Fim



















