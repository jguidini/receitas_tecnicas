############ - Atualizando Perl com CPAN
#
# Montada por Juliano - USP - RUSP - DTI - DVD - SCSC
#
### Objetivo: Atualizar o Perl utilizando CPAN
#
# Data Criação: 		Agosto/2014
# Data Última alteração:	01/Out/2014  jguidini
# 
# Versão: 1 - Utilizar o CPAN para atualizar o Perl.
#
# Criada em: Universidade de São Paulo - USP
# Reitoria da Universidade de São Paulo
# Departemento de Tecnologia da Informação
# Divisão de Datacenter
# Seção de Sistema Colaborativo
# Contato: scsc@usp.br
#
############ - Links - Inicio
#
# http://www.thegeekstuff.com/2008/09/how-to-install-perl-modules-manually-and-using-cpan-command/
#
############

# Usando o módulo do CPAN
perl -MCPAN -e shell

Será feito um setup caso nunca tenha usado esse módulo, pode escolher Não na primeira opção para escolher o mirror que deseja. Feito isso no prompt do perl digite:

# Isso atualiza o cache dos dados do CPAN
reload index

# Isso atualiza todos os módulos do Perl que estão instalados. Demora um pouco.. :-)
upgrade

# Para configurar a URL do repositório/mirror

o conf init urllist

# Para salvar as alterações

o conf commit

# Para instalar um módulo fora do promtp do CPAN
perl -MCPAN -e 'install Email::Reply' <- por exemplo

# Para instalar um módulo dentro do promtp do CPAN
install Email::Reply <- por exemplo

# Atualizar somente o CPAN

install CPAN
reload cpan

# Se um módulo não instalar sem force:
force install <modulo>

# Os módulos ficam armazenados em, note no fina a versão do Perl da sua máquina:
/usr/local/share/perl/5.10.1